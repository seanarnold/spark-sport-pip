function createPipButton() {
  if (document.getElementById("bitmovinplayer-video-null") != null) {
      if (document.getElementById("ss-pip-button") == null) {
        fullscreenButton = document.getElementsByClassName("bmpui-ui-fullscreentogglebutton")[0];
        newbuttonHtml = "<button type='button' id='ss-pip-button' class='bmpui-ui-fullscreentogglebutton bmpui-off'></button>";
        fullscreenButton.insertAdjacentHTML("beforebegin", newbuttonHtml);
        el = document.getElementById("ss-pip-button");
        el.addEventListener("click", pipClick, false);
      }
  } else {
    console.log("SS video player not found yet")
  }
}

function pipClick() {
  console.log("clicked");
  el = document.getElementById("bitmovinplayer-video-null");
  el.requestPictureInPicture();
}

setInterval(function(){ createPipButton(); }, 500);
