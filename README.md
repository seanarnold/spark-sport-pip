# Spark Sport PIP

A Chrome / Brave extension for enabling PIP on [Spark Sport](http://www.sparksport.co.nz) videos.

## Instructions

1. Head to `chrome://extensions`
1. Enable Developer mode
1. Use `Load Unpacked` and select the Spark Sport PIP dir
1. The extension should load automatically and enable a PIP button on the streams

![screenshot](images/screenshot.jpg)